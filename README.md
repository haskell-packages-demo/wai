# wai

Web Application Interface. https://www.stackage.org/package/wai

## Official documentation
* [*Web Application Interface*
  ](https://www.yesodweb.com/book/web-application-interface)
