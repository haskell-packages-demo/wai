{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Strict #-}

{-# LANGUAGE Trustworthy #-}
module Main (main) where

import Network.Wai
    ( Request
    , Response
    , responseLBS
    , ResponseReceived
    )
import safe Network.HTTP.Types (status200)
import Network.Wai.Handler.Warp (run)

application :: 
    Request ->
    (Response -> IO ResponseReceived) ->
    IO ResponseReceived
application _ respond = respond $
    responseLBS
        status200
        [("Content-Type", "text/plain")]
        "Hello World"

main :: IO ()
main = run 8000 application
