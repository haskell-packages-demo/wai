{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Main ( main ) where

import Basement.Block
    ( Block ) -- Has a builder but no null.

import Basement.Block.Builder qualified as BB ( Builder)

import Basement.Compat.Base
    ( Word8 )

import Basement.Compat.Typeable (Typeable) -- import Data.Typeable (Typeable)

-- import Basement.Imports qualified as BI ( String ) -- This is Text replacement not ByteString!

import Basement.UArray (UArray, null) -- Has null but no builder.

import Control.Monad
    ( unless
    , void
    )

-- from bytestring:
-- import Data.ByteString qualified as BS
    -- ( ByteString
    -- , hPut
    -- , null
    -- )

-- import Data.ByteString.Builder qualified as BB
    -- ( Builder
    -- , byteString
    -- , char7
    -- , lazyByteString
    -- , string8
    -- )

-- import Data.ByteString.Builder.Extra qualified as BBE (flush)

-- import Data.ByteString.Lazy qualified as BL
    -- (ByteString)

-- from case-insensitive:
import Data.CaseInsensitive qualified as CI
    (CI, original)

import Data.Function (fix)

import Data.List
    ( lookup -- Module ‘Foundation’, ‘Relude’ does not export ‘lookup’
    , map -- Module ‘Foundation’ does not export ‘map’
    )

-- from streaming-commons:
import Data.Streaming.ByteString.Builder qualified as StreamingBB
    ( defaultStrategy
    , newBuilderRecv
    )

import Foundation -- Basement.Compat.Base, Basement.Imports
    ( (.)
    , ($)
    , (>>=)
    , IO
    , mappend
    , Maybe( Just, Nothing )
    , maybe
    , mconcat
    , return
    , show
    )

-- import Foundation.Array.Boxed ()

-- import Foundation.Collection (null)

import Foundation.IO
    ( hPut
    , stdout
    )

import Network.HTTP.Types qualified as H
    ( hContentType
    , ResponseHeaders
    , Status (..) -- responseLBS, ...
    , status200
    )

-- import Network.Wai ()
    -- Application
    -- Request
    -- Response
    -- responseLBS
    -- ResponseReceived
    -- responseToStream

-- import Network.Wai.Handler.Warp (run)

-- import Network.Wai.Internal (Response( ResponseBuilder ))

-- import System.IO (stdout)

type ByteString = UArray Word8

-- Network.Wai.Internal (copied from)

data Request = Request
  deriving (Typeable)

data Response -- shortened
    = ResponseBuilder H.Status H.ResponseHeaders SB.Builder
  deriving Typeable

type StreamingBody = (SB.Builder -> IO ()) -> IO () -> IO ()

data ResponseReceived = ResponseReceived
    deriving Typeable

-- Network.Wai (copied from)

responseLBS :: H.Status -> H.ResponseHeaders -> BL.ByteString -> Response
responseLBS s h = ResponseBuilder s h . BB.lazyByteString

-- | Converting the body information in 'Response' to a 'StreamingBody'.
responseToStream :: Response
                ->  ( H.Status
                    , H.ResponseHeaders
                    , (StreamingBody -> IO ()) -> IO ()
                    )
responseToStream (ResponseBuilder status headers builder) =
    ( status
    , headers
    , \withBody ->
        (withBody :: StreamingBody -> IO ())
            $ \sendChunk _ ->
                (sendChunk :: SB.Builder -> IO())
                    builder
                :: IO()
        :: IO()
    )


type Application =
    Request ->
    (Response -> IO ResponseReceived) ->
    IO ResponseReceived


-- Demo application

application :: 
    Request ->
    (Response -> IO ResponseReceived) ->
    IO ResponseReceived
application _ respond = respond $
    responseLBS
        H.status200
        [("Content-Type", "text/plain")]
        "Hello World"


-- Network.Wai.Handler.CGI (copied and adapter from)

runGeneric
     :: Request
     -> (BI.String -> IO ()) -- ^ destination for output
     -> Application
     -> IO ()
runGeneric request outputH app = do
    void $ app request respond
  where
    respond :: Response -> IO ResponseReceived
    respond response =
        do
            let (httpStatus, responseHeaders, writeBody) = responseToStream response
            (blazeRecv, blazeFinish) <- StreamingBB.newBuilderRecv StreamingBB.defaultStrategy
            writeBody $ \b -> do
                let sendBuilder builder = do
                    popper <- blazeRecv builder
                    fix $ \loop -> do
                        bs <- popper
                        unless (BS.null bs) $ do
                            outputH bs
                            (loop :: IO ())
                        :: IO ()
                sendBuilder $ headers httpStatus responseHeaders `mappend` BB.char7 '\n'
                (b :: (SB.Builder -> IO ()) -> IO () -> IO ())
                    sendBuilder (sendBuilder BBE.flush)
                :: IO ()
            blazeFinish >>= maybe (return ()) outputH
            return ResponseReceived

    headers :: H.Status -> H.ResponseHeaders -> SB.Builder
    headers s hs = mconcat (map header $ status s : map header' (fixHeaders hs))

    status :: H.Status -> (SB.Builder, SB.Builder)
    status (H.Status i m) = (BB.byteString "Status", mconcat
        [ BB.string8 $ show i
        , BB.char7 ' '
        , BB.byteString m
        ])

    header'
        :: (CI.CI BI.String, BI.String)
        -> (SB.Builder, SB.Builder)
    header' (x, y) = (BB.byteString $ CI.original x, BB.byteString y)

    header
        :: (SB.Builder, SB.Builder)
        -> SB.Builder
    header (x, y) = mconcat
        [ x
        , BB.byteString ": "
        , y
        , BB.char7 '\n'
        ]

    fixHeaders
        :: H.ResponseHeaders
        -> H.ResponseHeaders
    fixHeaders h =
        case lookup H.hContentType h of
            Nothing -> (H.hContentType, "text/html; charset=utf-8") : h
            Just _ -> h


run :: Request -> Application -> IO ()
run request app = do
    let
        output :: BI.String -> IO ()
        output = BS.hPut stdout
    runGeneric request output app


-- Main

main :: IO ()
main = run Request application

